grammar cal;

program:                decl_list function_list main EOF;

decl_list:              decl SEMI_COLON decl_list
                        |
                        ;

decl:                   var_decl
                        | const_decl
                        ;

var_decl:               VARIABLE IDENTIFIER COLON type;

const_decl:             CONSTANT IDENTIFIER COLON type ASSIGN expression;

function_list:          function function_list
                        |
                        ;

function:               type IDENTIFIER LBR parameter_list RBR IS
                        decl_list
                        BEGIN
                        statement_block
                        RETURN LBR (expression | ) RBR SEMI_COLON
                        END
                        ;

type:                   INTEGER
                        | BOOLEAN
                        | VOID
                        ;

parameter_list:         nemp_parameter_list
                        |
                        ;

nemp_parameter_list:    IDENTIFIER COLON type                               # nemp_parameter
                        | IDENTIFIER COLON type COMMA nemp_parameter_list   # nemp_parameterlist
                        ;

main:                   MAIN
                        BEGIN
                        decl_list
                        statement_block
                        END
                        ;

statement_block:        statement statement_block                       # stms_block
                        |                                               # null_block
                        ;

statement:              IDENTIFIER ASSIGN expression SEMI_COLON         # assign_stm
                        | IDENTIFIER LBR arg_list RBR SEMI_COLON        # call_stm
                        | BEGIN statement_block END                     # block_stm
                        | IF condition BEGIN statement_block END        # if_stm
                        | ELSE BEGIN statement_block END                # else_stm
                        | WHILE condition BEGIN statement_block END     # while_stm
                        | SKIP_ SEMI_COLON                              # skip_stm
                        ;

expression:             fragment_ binary_arth_op fragment_              # arth_op_expr
                        | LBR expression RBR                            # br_expr
                        | IDENTIFIER LBR arg_list RBR                   # arg_lst_expr
                        | fragment_                                     # frg_expr
                        ;

binary_arth_op:         PLUS                                           
                        | MINUS                                         
                        ;

fragment_:              IDENTIFIER                                      # id_frag
                        | MINUS IDENTIFIER                              # minus_id_frag
                        | NUMBER                                        # no_frag
                        | TRUE                                          # true_frag
                        | FALSE                                         # false_frag
                        | fragment_ binary_arth_op fragment_            # arth_op_frag
                        | LBR expression RBR                            # br_exp_frag
                        | IDENTIFIER LBR arg_list RBR                   # arg_lst_frag
                        ;

condition:              NEG condition                                   # neg_condition
                        | LBR condition RBR                             # br_condition
                        | expression comp_op expression                 # comp_op_exp
                        | condition OR condition                        # or_condition
                        | condition AND condition                       # and_condition
                        ;

comp_op:                EQUAL | NOT_EQUAL | LESS_THAN | LESS_THAN_EQ | MORE_THAN | MORE_THAN_EQ;

arg_list:               nemp_arg_list
                        |
                        ;

nemp_arg_list:          IDENTIFIER                                      # nemp_arg
                        | IDENTIFIER COMMA nemp_arg_list                # nemp_arglist
                        ;

fragment V:             'v' | 'V';
fragment A:             'a' | 'A';
fragment R:             'r' | 'R';
fragment I:             'i' | 'I';
fragment B:             'b' | 'B';
fragment L:             'l' | 'L';
fragment E:             'e' | 'E';
fragment C:             'c' | 'C';
fragment O:             'o' | 'O';
fragment N:             'n' | 'N';
fragment S:             's' | 'S';
fragment T:             't' | 'T';
fragment U:             'u' | 'U';
fragment G:             'g' | 'G';
fragment D:             'd' | 'D';
fragment M:             'm' | 'M';
fragment F:             'f' | 'F';
fragment W:             'w' | 'W';
fragment H:             'h' | 'H';
fragment K:             'k' | 'K';
fragment P:             'p' | 'P';

fragment LETTER:        [a-zA-Z];
fragment DIGGIT:        [0-9];
fragment UNDERSCORE:    '_';

VARIABLE:               V A R I A B L E;
CONSTANT:               C O N S T A N T;
RETURN:                 R E T U R N;
INTEGER:                I N T E G E R;
BOOLEAN:                B O O L E A N;
VOID:                   V O I D;
MAIN:                   M A I N;
IF:                     I F;
ELSE:                   E L S E;
TRUE:                   T R U E;
FALSE:                  F A L S E;
WHILE:                  W H I L E;  
BEGIN:                  B E G I N;
END:                    E N D;
IS:                     I S;
SKIP_:                   S K I P;

ASSIGN:                 ':=';
COMMA:                  ',';
SEMI_COLON:             ';';
COLON:                  ':';
LBR:                    '(';
RBR:                    ')';
PLUS:                   '+';
MINUS:                  '-';
NEG:                    '~';
OR:                     '|';
AND:                    '&';
EQUAL:                  '=';
NOT_EQUAL:              '!=';
LESS_THAN:              '<';
LESS_THAN_EQ:           '<=';
MORE_THAN:              '>';
MORE_THAN_EQ:           '>=';

IDENTIFIER:             LETTER(LETTER | DIGGIT | UNDERSCORE)*;
NUMBER:                 ('-')*([1-9][0-9]*)|'0';

COMMENT:                '//' ~[\r\n]* -> skip;
BLOCKCOMMENT:           '/*' (BLOCKCOMMENT|.)*? '*/' -> skip;
WS:			            [ \t\n\r]+ -> skip;
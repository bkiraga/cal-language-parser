import java.util.*;
import java.util.Hashtable;
import java.util.Stack;

public class SymbolTable {
    Stack<Hashtable<String, String>> stack;

    SymbolTable(){
        this.stack = new Stack<Hashtable<String,String>>();
    }

    public void openScope(){
        stack.push(new Hashtable<String, String>());
    }

    public void closeScope(){
        stack.pop();
    }

    public void add(String id, String type){
        stack.peek().put(id, type);
    }

    public String lookUp(String id){
        for(int i = stack.size()-1; i >= 0; i--){
            String type = stack.elementAt(i).get(id);
            if (type != null){
                return type;
            }
        }
        return null;
    }

    public String lookUpInScope(String id) {
        return stack.peek().get(id);
    }

    public Boolean isDuplicate(String id) {
        for(int i = stack.size()-1; i >= 0; i--){
            if(stack.elementAt(i).containsKey(id)){
                return true;
            }
        }
        return false;
    }

    public Boolean isDuplicateInScope(String id){
        if(stack.peek().containsKey(id)){
            return true;
        }
        return false;
    }

    public String toString(){
        String str = "";
        for(int i = stack.size() - 1, j = stack.size() -1; i >= 0; i--, j--){
            str = str + "Scope " + j + ": " + stack.elementAt(i) + "\n";
        }
        return str;
    }
}
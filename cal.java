import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.CharStreams;
import java.io.FileInputStream;
import java.io.InputStream;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import java.lang.ArrayIndexOutOfBoundsException;

public class cal {
    public static void main(String[] args) throws Exception{

        String inputFile = null;

	    if (args.length > 0)
	        inputFile = args[0];

	    InputStream is = System.in;
	    if (inputFile != null)
	        is = new FileInputStream(inputFile);

	    calLexer lexer = new calLexer(CharStreams.fromStream(is));
	    CommonTokenStream tokens = new CommonTokenStream(lexer);
	    calParser parser = new calParser(tokens);

        // code used from line 21-44 taken from https://stackoverflow.com/questions/39533809/antlr4-how-to-detect-unrecognized-token-and-given-sentence-is-invalid
        parser.setErrorHandler(new DefaultErrorStrategy() {

            @Override
            public void recover(Parser recognizer, RecognitionException e) {
                for (ParserRuleContext context = recognizer.getContext(); context != null; context = context.getParent()) {
                    context.exception = e;
                }

                throw new ParseCancellationException(e);
            }


            @Override
            public Token recoverInline(Parser recognizer)
                throws RecognitionException
            {
                InputMismatchException e = new InputMismatchException(recognizer);
                for (ParserRuleContext context = recognizer.getContext(); context != null; context = context.getParent()) {
                    context.exception = e;
                }

                throw new ParseCancellationException(e);
            }
        });

        try {
            ParseTree parseTree = parser.program();
            System.out.println(args[0] + " parsed successfully");
            calSemanticVisitor calV = new calSemanticVisitor();
            calV.visit(parseTree);
        }
        catch(ParseCancellationException e){
            System.out.println(args[0] + " has not parsed");
        }
    }
}
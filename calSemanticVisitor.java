import java.beans.Expression;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.Arrays;
import java.lang.Exception;

public class calSemanticVisitor extends calBaseVisitor<String> {

    SymbolTable symbolTable;
    int errorCount;

    calSemanticVisitor(){
        this.symbolTable = new SymbolTable();
        this.errorCount = 0;
    }

    @Override
    public String visitProgram(calParser.ProgramContext ctx){
        symbolTable.openScope();
        visitChildren(ctx);
        symbolTable.closeScope();
        if(this.errorCount == 0){
            System.out.println("Semantic Analysis Successful!");
        }
        else{
            System.out.println("Semantic Analysis has Failed!" + "\n" + this.errorCount + " error/s found");
        }
        return "";
    }

    @Override
    public String visitVar_decl(calParser.Var_declContext ctx){
        String id = ctx.getChild(1).getText();
        String type = visit(ctx.getChild(3));
        if(symbolTable.isDuplicateInScope(id)){
            System.out.println("Error: " + id + " already defined");
            this.errorCount += 1;
        }
        else{
            symbolTable.add(id, type);
        }
        return type;
    }

    @Override
    public String visitConst_decl(calParser.Const_declContext ctx){
        String id = ctx.getChild(1).getText();
        String type = visit(ctx.getChild(3));
        String expr = visit(ctx.expression());
        if(symbolTable.isDuplicateInScope(id)){
            System.out.println("Error: " + id + " already defined");
            this.errorCount += 1;
        }
        else if(type.equals(expr)){
            symbolTable.add(id, type);
            symbolTable.add(id + "/const", type);
        }
        else{
            System.out.println("Error: " + expr + " is of wrong type");
            this.errorCount += 1;
        }
        return type;
    }

    @Override
    public String visitFunction(calParser.FunctionContext ctx){
        String id = ctx.IDENTIFIER().getText();
        String type = visit(ctx.type());
        String params = visit(ctx.parameter_list());
        String allFunctionTypes = type + "," + params;
        symbolTable.add(id, type);
        symbolTable.add(id + "/args", allFunctionTypes);
        symbolTable.openScope();
        String[] paramArray = params.split(",");
        for(int i = 0; i < paramArray.length; i++){
            symbolTable.add(paramArray[i].split(":")[0], paramArray[i].split(":")[1]);
        }
        visit(ctx.decl_list());
        visit(ctx.statement_block());
        String returnValue = visit(ctx.getChild(11));
        if(!(symbolTable.lookUp(returnValue).equals(type))){
            System.out.println("Error: return type does not match");
            this.errorCount += 1;
        }
        symbolTable.closeScope();    
        return type;
    }

    @Override
    public String visitType(calParser.TypeContext ctx){
        String type = ctx.getChild(0).getText().toUpperCase();
        return type;
    }

    @Override
    public String visitNemp_parameterlist(calParser.Nemp_parameterlistContext ctx){
        String id = ctx.IDENTIFIER().getText();
        String type = visit(ctx.type());
        String next = visit(ctx.nemp_parameter_list());
        String params = id + ":" + type + "," + next;
        return params;
    }

    @Override
    public String visitNemp_parameter(calParser.Nemp_parameterContext ctx){
        String id = ctx.IDENTIFIER().getText();
        String type = visit(ctx.type());
        String param = id + ":" + type;
        return param;
    }

    @Override
    public String visitMain(calParser.MainContext ctx){
        symbolTable.openScope();
        visitChildren(ctx);
        symbolTable.closeScope();
        return "";
    }

    @Override
    public String visitStms_block(calParser.Stms_blockContext ctx){
        String stm = visit(ctx.getChild(0));
        String next = visit(ctx.getChild(1));
        String stms = stm + ";" + next;
        return stms;
    }

    @Override
    public String visitAssign_stm(calParser.Assign_stmContext ctx){
        String id = ctx.IDENTIFIER().getText();
        String expr = visit(ctx.expression());
        if(symbolTable.isDuplicate(expr)){
            expr = symbolTable.lookUp(expr);
        }
        if(!(symbolTable.isDuplicate(id))){
            System.out.println("Error: undefined variable");
            this.errorCount += 1;
        }
        if(symbolTable.isDuplicate(id + "/const")){
            System.out.println("Error cannot assign to constants");
            this.errorCount += 1;
        }
        if(!(symbolTable.lookUp(id).equals(expr))){
            System.out.println("Error: type missmatch");
            this.errorCount += 1;
        }
        else{
            symbolTable.add(id, expr);
        }
        return expr;
    }

    @Override
    public String visitCall_stm(calParser.Call_stmContext ctx){
        String id = ctx.IDENTIFIER().getText();
        String args = visit(ctx.arg_list());

        if(symbolTable.isDuplicate(id)){
            String funcArguments = symbolTable.lookUp(id + "/args");
            String[] funcArray = funcArguments.split(",");
            String[] argArray = args.split(",");
            if(!(argArray.length == funcArray.length-1)){
                System.out.println("Error: wrong number of arguments for a function call");
                this.errorCount += 1;
            }
            return funcArray[0];
        }
        else{
            System.out.println("Error: function not declared");
            this.errorCount += 1;
        }
        return "";    
    }

    @Override
    public String visitArth_op_expr(calParser.Arth_op_exprContext ctx){
        String left = visit(ctx.fragment_(0));
        String right = visit(ctx.fragment_(1));
        if(symbolTable.isDuplicate(left)){
            left = symbolTable.lookUp(left);
        }
        if(symbolTable.isDuplicate(right)){
            right = symbolTable.lookUp(right);
        }
        if(!(left.equals(right) && left.equals("INTEGER"))){
            System.out.println("Error arguments must be integers");
            this.errorCount += 1;
        }
        return left;
    }

    @Override
    public String visitBr_expr(calParser.Br_exprContext ctx){
        return visit(ctx.expression());
    }

    @Override
    public String visitArg_lst_expr(calParser.Arg_lst_exprContext ctx){
        String id = ctx.IDENTIFIER().getText();
        String args = visit(ctx.arg_list());
        String cal = id + ":" + args;
        if(symbolTable.isDuplicate(id)){
            String funcArguments = symbolTable.lookUp(id + "/args");
            String[] funcArray = funcArguments.split(",");
            String[] argArray = args.split(",");
            if(!(argArray.length == funcArray.length-1)){
                System.out.println("Error: wrong number of arguments for a function call");
                this.errorCount += 1;
            }
            return funcArray[0];
        }
        else{
            System.out.println("Error: function not declared");
            this.errorCount += 1;
        }
        return "";
    }

    @Override
    public String visitId_frag(calParser.Id_fragContext ctx){
        String id = ctx.getChild(0).getText();
        if(!(symbolTable.isDuplicate(id))){
            System.out.println("Error: undeclared identifier");
            this.errorCount += 1;
        }
        return id;
    }

    @Override
    public String visitMinus_id_frag(calParser.Minus_id_fragContext ctx){
        String id = ctx.IDENTIFIER().getText();
        if(!(symbolTable.isDuplicate(id))){
            System.out.println("Error: undeclared identifier");
            this.errorCount += 1;
        }
        if(!(symbolTable.lookUp(id).equals("INTEGER"))){
            System.out.println("Error: minus sign reserved for integer");
            this.errorCount += 1;
        }
        return id;
    }

    @Override
    public String visitNo_frag(calParser.No_fragContext ctx){
        return "INTEGER";
    }

    @Override
    public String visitTrue_frag(calParser.True_fragContext ctx){
        return "BOOLEAN";
    }

    @Override
    public String visitFalse_frag(calParser.False_fragContext ctx){
        return "BOOLEAN";
    }

    @Override
    public String visitComp_op_exp(calParser.Comp_op_expContext ctx){
        String left = visit(ctx.expression(0));
        String right = visit(ctx.expression(1));
        if(!(symbolTable.lookUp(left).equals(right))){
            System.out.println("Error: types must match");
            this.errorCount += 1;
        }
        return "BOOLEAN";
    }

    @Override
    public String visitOr_condition(calParser.Or_conditionContext ctx){
        String left = visit(ctx.condition(0));
        String right = visit(ctx.condition(1));
        if(!(left.equals(right) && left.equals("BOOLEAN"))){
            System.out.println("Error arguments must be boolean");
            this.errorCount += 1;
        }
        return "BOOLEAN";
    }

    @Override
    public String visitAnd_condition(calParser.And_conditionContext ctx){
        String left = visit(ctx.condition(0));
        String right = visit(ctx.condition(1));

        if(!(left.equals(right) && left.equals("BOOLEAN"))){
            System.out.println("Error arguments must be boolean");
            this.errorCount += 1;
        }
        return "BOOLEAN";
    }

    @Override
    public String visitBr_condition(calParser.Br_conditionContext ctx){
        return visit(ctx.condition());
    }

    @Override
    public String visitNemp_arglist(calParser.Nemp_arglistContext ctx){
        String id = ctx.IDENTIFIER().getText();
        String next = visit(ctx.nemp_arg_list());
        String args = id + "," + next;
        return args;
    }

    @Override
    public String visitNemp_arg(calParser.Nemp_argContext ctx){
        String id = ctx.IDENTIFIER().getText();
        String arg = id;
        return arg;
    }
}